package com.underwater.dron;

/**
 * @author gektor650
 * @since 2/4/18
 */

public class GeometryUtils {
    
    public static int getAngle(float x1, float y1, float x2, float y2) {
        return (int) (Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI);
    }

    public static double getDistance(float x1, float y1, float x2, float y2) {
        return Math.hypot(x1 - x2, y1 - y2);
    }
}
