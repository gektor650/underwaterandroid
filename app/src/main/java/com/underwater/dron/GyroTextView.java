package com.underwater.dron;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * @author gektor650
 * @since 1/21/18
 */

public class GyroTextView extends RelativeLayout {
    private TextView mXView;
    private TextView mYView;
    private TextView mZView;
    private TextView mAccelXView;
    private TextView mAccelYView;
    private TextView mAccelZView;
    private TextView mRotationXView;
    private TextView mRotationYView;
    private TextView mRotationZView;
    private TextView mGyroTempView;

    public GyroTextView(Context context) {
        super(context);
    }

    public GyroTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public GyroTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mXView = findViewById(R.id.x_value);
        mYView = findViewById(R.id.y_value);
        mZView = findViewById(R.id.z_value);

        mAccelXView = findViewById(R.id.accel_x_value);
        mAccelYView = findViewById(R.id.accel_y_value);
        mAccelZView = findViewById(R.id.accel_z_value);

        mRotationXView = findViewById(R.id.rotation_x_value);
        mRotationYView = findViewById(R.id.rotation_y_value);
        mRotationZView = findViewById(R.id.rotation_z_value);

        mGyroTempView = findViewById(R.id.gyro_temp);
    }

    public void setGyroData(float gyroX, float gyroY, float gyroZ) {
        Resources resources = getResources();
        String x = resources.getString(R.string.gyro_x, gyroX);
        String y = resources.getString(R.string.gyro_y, gyroY);
        String z = resources.getString(R.string.gyro_z, gyroZ);
        mXView.setText(x);
        mYView.setText(y);
        mZView.setText(z);
    }

    public void setAccelData(float x, float y, float z) {
        Resources resources = getResources();
        String ax = resources.getString(R.string.accel_x, x);
        String ay = resources.getString(R.string.accel_y, y);
        String az = resources.getString(R.string.accel_z, z);
        mAccelXView.setText(ax);
        mAccelYView.setText(ay);
        mAccelZView.setText(az);
    }

    public void setRotationData(float x, float y, float z) {
        Resources resources = getResources();
        String ax = resources.getString(R.string.rotation_x, x);
        String ay = resources.getString(R.string.rotation_y, y);
        String az = resources.getString(R.string.rotation_z, z);
        mRotationXView.setText(ax);
        mRotationYView.setText(ay);
        mRotationZView.setText(az);
    }

    public void setTempData(float t) {
        Resources resources = getResources();
        String ax = resources.getString(R.string.gyro_temp, t);
        mGyroTempView.setText(ax);
    }
}
