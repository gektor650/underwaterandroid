package com.underwater.dron;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;

/**
 * @author gektor650
 * @since 11/9/17.
 */

public class JoystickView extends View {

    private final SparseArray<PointerData> mPointerDataList = new SparseArray<>();
    private final Paint mCirclePaint = new Paint();
    private final Paint mLinePaint = new Paint();
    private int mCircleRadius;
    private OnMoveListener mOnMoveListener;
    private double mMaxValuePixels;

    public JoystickView(Context context) {
        super(context);
    }

    public JoystickView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public JoystickView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        int circleColor = ContextCompat.getColor(getContext(), R.color.joystickPointerColor);
        int lineColor = ContextCompat.getColor(getContext(), R.color.joystickLineColor);
        float lineWidth = getResources().getDimensionPixelOffset(R.dimen.joystickLine);
        mCircleRadius = getResources().getDimensionPixelOffset(R.dimen.joystickCircleWidth);
        mCirclePaint.setColor(circleColor);
        mCirclePaint.setAntiAlias(true);
        mLinePaint.setColor(lineColor);
        mLinePaint.setAntiAlias(true);
        mLinePaint.setStrokeWidth(lineWidth);
    }

    @Override
    public boolean onCapturedPointerEvent(MotionEvent event) {
        System.out.println(event);
        return super.onCapturedPointerEvent(event);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mMaxValuePixels = h / 3;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        int index = event.getActionIndex();
        int action = event.getActionMasked();
        int pointerId = event.getPointerId(index);

        int pointerIndex = event.findPointerIndex(pointerId);
        float x = event.getX(pointerIndex);
        float y = event.getY(pointerIndex);

        PointerData pointerData = getPointer(pointerId, x);

        if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP) {
            PointerData lastData = mPointerDataList.get(pointerId);
            if(mOnMoveListener != null && lastData != null) {
                mOnMoveListener.onMove(lastData.mStartPosition, 0, 0);
            }
            mPointerDataList.remove(pointerId);
        } else if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN) {
            pointerData.mStartX = x;
            pointerData.mStartY = y;
            pointerData.mCurrentX = x;
            pointerData.mCurrentY = y;
        } else if (action == MotionEvent.ACTION_MOVE) {
            int pointerCount = event.getPointerCount();
            for (int i = 0; i < pointerCount; ++i) {
                int pid = event.getPointerId(i);
                PointerData pointerD = getPointer(pid, 0);
                pointerD.mCurrentX = event.getX(i);
                pointerD.mCurrentY = event.getY(i);
            }
        }
        notifyCallback();
        invalidate();
        return true;
    }

    private void notifyCallback() {
        if (mOnMoveListener != null && mMaxValuePixels != 0) {
            for (int i = 0; i < mPointerDataList.size(); i++) {
                int key = mPointerDataList.keyAt(i);
                PointerData data = mPointerDataList.get(key);
                int angle = GeometryUtils.getAngle(data.mStartX, data.mStartY, data.mCurrentX, data.mCurrentY);
                double distance = GeometryUtils.getDistance(data.mStartX, data.mStartY, data.mCurrentX, data.mCurrentY);
                int val = (int) ((100f / mMaxValuePixels) * distance);
                if (val < 0) val = 0;
                if (val > 100) val = 100;
                mOnMoveListener.onMove(data.mStartPosition, angle, val);
            }
        }
    }

    private PointerData getPointer(int pointerIndex, float x) {
        PointerData pointerData = mPointerDataList.get(pointerIndex);
        if (pointerData == null) {
            pointerData = new PointerData();
            if (x > getWidth() / 2) pointerData.mStartPosition = Position.RIGHT;
            mPointerDataList.put(pointerIndex, pointerData);
        }
        return pointerData;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < mPointerDataList.size(); i++) {
            int key = mPointerDataList.keyAt(i);
            PointerData data = mPointerDataList.get(key);
            canvas.drawCircle(data.mCurrentX, data.mCurrentY, mCircleRadius, mCirclePaint);
            canvas.drawLine(data.mStartX, data.mStartY, data.mCurrentX, data.mCurrentY, mLinePaint);
        }
    }

    public void setOnMoveListener(OnMoveListener onMoveListener) {
        this.mOnMoveListener = onMoveListener;
    }

    private class PointerData {
        private Position mStartPosition = Position.LEFT;
        private float mStartX;
        private float mStartY;
        private float mCurrentX;
        private float mCurrentY;
    }

    public interface OnMoveListener {
        void onMove(Position position, int angle, int value);
    }

    public enum Position {
        LEFT("L"), RIGHT("R");

        private final String position;

        Position(String position) {
            this.position = position;
        }

        @Override
        public String toString() {
            return position;
        }
    }
}
