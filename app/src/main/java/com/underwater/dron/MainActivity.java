package com.underwater.dron;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.Log;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.underwater.dron.classes.SensorsState;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = "DPAD";
    private SocketThread mSocketThread;
    private MutableLiveData<SocketThread.SocketState> mSocketState = new MutableLiveData<>();
    private MutableLiveData<SensorsState> mSensorState = new MutableLiveData<>();
    private View mErrorView;
    private TextView mTemperatureView;
    private GyroTextView mGyroView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mErrorView = findViewById(R.id.error);
        mGyroView = findViewById(R.id.gyroscope);
        mTemperatureView = findViewById(R.id.temperature_value);
        TextView testMode = findViewById(R.id.test_mode);
        AppCompatSeekBar mLightBar = findViewById(R.id.light);

        JoystickView jostick = findViewById(R.id.joystick);
        jostick.setOnMoveListener(new JoystickView.OnMoveListener() {

            @Override
            public void onMove(JoystickView.Position position, int angle, int value) {
                mSocketThread.send(position.toString(), angle, value);
            }
        });

        testMode.setOnClickListener(this);

        mLightBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mSocketThread.sendLight(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        mSocketState.observe(this, new Observer<SocketThread.SocketState>() {
            @Override
            public void onChanged(@Nullable SocketThread.SocketState socketState) {
                if (socketState != null) {
                    if (socketState == SocketThread.SocketState.CONNECTED) {
                        mErrorView.setVisibility(View.GONE);
                    } else {
                        mErrorView.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        final int errorColor = ContextCompat.getColor(this, R.color.errorText);
        final int usualColor = ContextCompat.getColor(this, R.color.colorText);
        mSensorState.observe(this, new Observer<SensorsState>() {
            @Override
            public void onChanged(@Nullable SensorsState state) {
                if (state != null && mTemperatureView != null && mGyroView != null) {
                    float temperature = state.getTemperature();
                    if (temperature > 60) {
                        mTemperatureView.setTextColor(errorColor);
                    } else {
                        mTemperatureView.setTextColor(usualColor);
                    }
                    String tempText = getString(R.string.temperature, temperature);
                    mTemperatureView.setText(tempText);
                    SensorData sensorData = state.getGyroscope();
                    if (sensorData != null) {
                        mGyroView.setGyroData(sensorData.getGyroX(), sensorData.getGyroY(), sensorData.getGyroZ());
                        mGyroView.setAccelData(sensorData.getAccelX(), sensorData.getAccelY(), sensorData.getAccelZ());
                        mGyroView.setRotationData(sensorData.getRotationX(), sensorData.getRotationY(), sensorData.getRotationZ());
                        mGyroView.setTempData(sensorData.getTemperature());
                    }
                }
            }
        });

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new VideoFragment(), VideoFragment.class.getSimpleName()).commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSocketThread = new SocketThread(mSocketState, mSensorState);
        mSocketThread.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSocketThread != null) {
            mSocketThread.interrupt();
            mSocketThread = null;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.test_mode) {
            mSocketThread.send(JoystickView.Position.LEFT.toString(), -90, 100);
            mSocketThread.send(JoystickView.Position.RIGHT.toString(), -90, 100);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean handled = false;
        if ((event.getSource() & InputDevice.SOURCE_GAMEPAD)
                == InputDevice.SOURCE_GAMEPAD) {
            if (event.getRepeatCount() == 0) {
                switch (keyCode) {
                    // Handle gamepad and D-pad button presses to
                    // navigate the ship
                    default:
                        if (isFireKey(keyCode)) {
                            handled = true;
                        }
                        break;
                }
            }
        }
        return handled || super.onKeyDown(keyCode, event);
    }

    private boolean isFireKey(int keyCode) {
        // Here we treat Button_A and DPAD_CENTER as the primary action
        // keys for the game.
        switch (keyCode) {
            case KeyEvent.KEYCODE_BUTTON_L1:
                mSocketThread.send(JoystickView.Position.LEFT.toString(), -90, 100);
                mSocketThread.send(JoystickView.Position.RIGHT.toString(), -90, 100);

                return true;

            case KeyEvent.KEYCODE_BUTTON_L2:
                mSocketThread.send(JoystickView.Position.LEFT.toString(), 0, 0);
                mSocketThread.send(JoystickView.Position.RIGHT.toString(), 0, 0);
                return true;

        }
        return keyCode == KeyEvent.KEYCODE_DPAD_CENTER
                || keyCode == KeyEvent.KEYCODE_BUTTON_A
                || keyCode == KeyEvent.KEYCODE_BUTTON_B
                || keyCode == KeyEvent.KEYCODE_BUTTON_X
                || keyCode == KeyEvent.KEYCODE_BUTTON_Y
                ;
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        // Check that the event came from a game controller
        if ((event.getSource() & InputDevice.SOURCE_JOYSTICK) ==
                InputDevice.SOURCE_JOYSTICK &&
                event.getAction() == MotionEvent.ACTION_MOVE) {

            // Process all historical movement samples in the batch
            final int historySize = event.getHistorySize();

            // Process the movements starting from the
            // earliest historical position in the batch
            for (int i = 0; i < historySize; i++) {
                // Process the event at historical position i
                processJoystickInput(event, i);
            }

            // Process the current movement sample in the batch (position -1)
            processJoystickInput(event, -1);
            return true;
        }
        return super.onGenericMotionEvent(event);
    }

    private static float getCenteredAxis(MotionEvent event,
                                         InputDevice device, int axis, int historyPos) {
        final InputDevice.MotionRange range =
                device.getMotionRange(axis, event.getSource());

        // A joystick at rest does not always report an absolute position of
        // (0,0). Use the getFlat() method to determine the range of values
        // bounding the joystick axis center.
        if (range != null) {
            final float flat = range.getFlat();
            final float value =
                    historyPos < 0 ? event.getAxisValue(axis) :
                            event.getHistoricalAxisValue(axis, historyPos);

            // Ignore axis values that are within the 'flat' region of the
            // joystick axis center.
            if (Math.abs(value) > flat) {
                return value;
            }
        }
        return 0;
    }

    private void processJoystickInput(MotionEvent event,
                                      int historyPos) {

        InputDevice mInputDevice = event.getDevice();

        float lx, ly, rx, ry;
        // Calculate the horizontal distance to move by
        // using the input value from one of these physical controls:
        // the left control stick, hat axis, or the right control stick.
        lx = getCenteredAxis(event, mInputDevice,
                MotionEvent.AXIS_X, historyPos);
        if (lx == 0) {
            lx = getCenteredAxis(event, mInputDevice,
                    MotionEvent.AXIS_HAT_X, historyPos);
        }
        rx = getCenteredAxis(event, mInputDevice,
                MotionEvent.AXIS_Z, historyPos);
        // Calculate the vertical distance to move by
        // using the input value from one of these physical controls:
        // the left control stick, hat switch, or the right control stick.
        ly = getCenteredAxis(event, mInputDevice,
                MotionEvent.AXIS_Y, historyPos);
        if (ly == 0) {
            ly = getCenteredAxis(event, mInputDevice,
                    MotionEvent.AXIS_HAT_Y, historyPos);
        }
        ry = getCenteredAxis(event, mInputDevice,
                MotionEvent.AXIS_RZ, historyPos);

        float triggerL = getCenteredAxis(event, mInputDevice,
                MotionEvent.AXIS_GAS, historyPos);

        float triggerR = getCenteredAxis(event, mInputDevice,
                MotionEvent.AXIS_BRAKE, historyPos);
        Log.d(TAG, "triggerL " + triggerL + " triggerR " + triggerR);

        int angle = GeometryUtils.getAngle(0, 0, lx, ly);
        double distance = GeometryUtils.getDistance(0, 0, lx, ly);
        int val = (int) (100f * distance);
        // Update the ship object based on the new x and y values
        Log.d(TAG, "POSITION: " + JoystickView.Position.LEFT.toString() + " angle: " + angle + " val: " + val);
        mSocketThread.send(JoystickView.Position.LEFT.toString(), angle, val);

        angle = GeometryUtils.getAngle(0, 0, rx, ry);
        distance = GeometryUtils.getDistance(0, 0, rx, ry);
        val = (int) (100f * distance);
        mSocketThread.send(JoystickView.Position.RIGHT.toString(), angle, val);
        // Update the ship object based on the new x and y values
        Log.d(TAG, "POSITION: " + JoystickView.Position.RIGHT.toString() + " angle: " + angle + " val: " + val);
    }

}
