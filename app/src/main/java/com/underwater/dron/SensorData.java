package com.underwater.dron;

/**
 * @author gektor650
 * @since 1/21/18
 */

public class SensorData {

    private Coordinates gyro;
    private Coordinates accel;
    private Coordinates rotation;
    private float temp;

    public float getGyroX() {
        return (gyro != null) ? gyro.x: 0;
    }

    public float getGyroY() {
        return (gyro != null) ? gyro.y: 0;
    }

    public float getGyroZ() {
        return (gyro != null) ? gyro.z: 0;
    }

    public float getTemperature() {
        return temp;
    }

    public float getAccelX() {
        return (accel != null) ? accel.x: 0;
    }


    public float getAccelY() {
        return (accel != null) ? accel.y: 0;
    }


    public float getAccelZ() {
        return (accel != null) ? accel.z: 0;
    }

    public float getRotationX() {
        return (rotation != null) ? rotation.x: 0;
    }

    public float getRotationY() {
        return (rotation != null) ? rotation.y: 0;
    }

    public float getRotationZ() {
        return (rotation != null) ? rotation.z: 0;
    }

    private class Coordinates {
        private float x;
        private float y;
        private float z;
    }

}
