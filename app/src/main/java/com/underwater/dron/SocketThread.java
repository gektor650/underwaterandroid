package com.underwater.dron;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.AnyThread;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.underwater.dron.classes.SensorsState;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author gektor650
 * @since 10/21/17
 */

class SocketThread extends Thread {

    private static final String TAG = SocketThread.class.getSimpleName();
    private static final int PING_TIMEOUT = 1000;
    private static final int DEFAULT_TIMEOUT = 2000;

    private final SensorsState mSensorsState = new SensorsState();

    //Command constants
    private static final String PING = "PING";
    private static final String ARG_COMMAND = "C:";
    private static final String ARG_LIGHT = "LIGHT";
    private static final String ARG_SENSOR = "S";
    private static final String ARG_ANGLE = "A:";
    private static final String ARG_TEMPERATURE = "T:";
    private static final String ARG_VALUE = "V:";
    private static final String DELIMITER = ";";
    private static final String VAL_DELIMITER = ":";

    //Messages queue
    private final BlockingQueue<String> mCommandQueue = new LinkedBlockingQueue<>();

    //The time of last ping
    private long mLastPing;
    //The time of last pong
    private long mLastPong;

    //Socket state data. View will listen it
    @NonNull
    private final MutableLiveData<SocketState> mSocketCallback;
    //Sensor state data. View will listen it
    @NonNull
    private final MutableLiveData<SensorsState> mSensorCallback;

    //Readers for data

    @Nullable
    private BufferedReader mReader;
    @Nullable
    private PrintWriter mWriter;
    @Nullable
    private Socket mSocket;

    public enum SocketState {
        CONNECTED, DISCONNECTED
    }

    SocketThread(@NonNull MutableLiveData<SocketState> state,@NonNull MutableLiveData<SensorsState> sensorsStateMutableLiveData) {
        mSocketCallback = state;
        mSensorCallback = sensorsStateMutableLiveData;
        mSensorCallback.setValue(mSensorsState);
        mLastPong = System.currentTimeMillis();
    }

    @Override
    public void run() {
        super.run();
        //Reconnection delay. It will be increased by each unsuccessful connect
        int reconnectionDelay = 1000;

        //Try to connect while not interrupted
        while (!isInterrupted()) {
            try {
                //Create new socket and try to connect to IP:PORT with default timeout
                mSocket = new Socket();
                mSocket.connect(new InetSocketAddress(BuildConfig.IP, BuildConfig.NODE_PORT), DEFAULT_TIMEOUT);
                if (mSocket.isConnected()) {
                    OutputStream out = mSocket.getOutputStream();
                    InputStream in = mSocket.getInputStream();
                    mReader = new BufferedReader(new InputStreamReader(in));
                    mWriter = new PrintWriter(out, true);
                    Log.d(TAG, "connected:" + BuildConfig.IP + ":" + BuildConfig.NODE_PORT);
                    //Update view about socket state
                    mSocketCallback.postValue(SocketState.CONNECTED);
                    while (!isInterrupted()) {
                        String command;
                        if (mReader.ready()) {
                            command = mReader.readLine();
                            mLastPong = System.currentTimeMillis();
                            if (!Utils.isEmpty(command)) {
                                Log.d(TAG, command);
                                parseInputCommand(command);
                            }
                        }

                        if (!mCommandQueue.isEmpty()) {
                            mWriter.println(mCommandQueue.poll());
                            mWriter.flush();
                        }
                        long time = System.currentTimeMillis();
                        if (time - mLastPing > PING_TIMEOUT) {
                            mLastPing = time;
                            mWriter.println(PING);
                            mWriter.flush();
                        }
                        if (time - mLastPong > PING_TIMEOUT * 10) {
                            throw new IOException("We don't receive pong.");
                        }
                    }
                }
            } catch (IOException e) {
                Log.d(TAG, "IOException:" + BuildConfig.IP + ":" + BuildConfig.NODE_PORT + " " + e.getLocalizedMessage());
                mSocketCallback.postValue(SocketState.DISCONNECTED);
            } finally {
                mLastPong = System.currentTimeMillis();
                close();
            }
            try {
                sleep(reconnectionDelay);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
        close();
    }

    private void parseInputCommand(@NonNull String command) {
        if (command.startsWith(ARG_COMMAND)) {
            if (command.startsWith(ARG_SENSOR, 2)) {
                String jsonString = command.substring(4, command.indexOf(DELIMITER, 4));
                mSensorsState.setGyroscope(jsonString);
                mSensorCallback.postValue(mSensorsState);
            } else {
                String[] responses = command.split(DELIMITER);
                for (String response : responses) {
                    if (response.startsWith(ARG_TEMPERATURE)) {
                        String val[] = response.split(VAL_DELIMITER);
                        if (val.length > 1) {
                            try {
                                mSensorsState.setTemperature(Float.valueOf(val[1]));
                                mSensorCallback.postValue(mSensorsState);
                            } catch (NumberFormatException ignored) {

                            }
                        }
                    }
                }
            }
        }
    }

    private void close() {
        try {
            if (mSocket != null) {
                mSocket.close();
                mSocket = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (mReader != null) {
                mReader.close();
                mReader = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (mWriter != null) {
            mWriter.close();
            mWriter = null;
        }
    }

    @AnyThread
    void send(String position, @IntRange(from = -180, to = 180) int angle, @IntRange(from = 0, to = 100) int strength) {
        //Add command to Queue. Angle -180 180, strength 0-100
        mCommandQueue.add(ARG_COMMAND + position + DELIMITER + ARG_ANGLE + angle + DELIMITER + ARG_VALUE + strength + DELIMITER);
    }

    @AnyThread
    void sendLight(@IntRange(from = 0, to = 100) int progress) {
        mCommandQueue.add(ARG_COMMAND + ARG_LIGHT + DELIMITER + ARG_VALUE + progress + DELIMITER);
    }

}
