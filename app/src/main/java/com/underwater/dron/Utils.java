package com.underwater.dron;

/**
 * @author gektor650
 * @since 11/12/17
 */

class Utils {
    private Utils(){};

    static boolean isEmpty(String string) {
        return string == null || string.isEmpty();
    }
}
