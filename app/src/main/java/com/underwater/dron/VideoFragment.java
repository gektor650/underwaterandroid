package com.underwater.dron;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.underwater.dron.classes.DecoderThread;


public class VideoFragment extends Fragment implements TextureView.SurfaceTextureListener {

    private DecoderThread mDecoderThread;
    private final MutableLiveData<DecoderThread.State> mSocketState = new MutableLiveData<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video, container, false);
    }

    //mplayer -fps 200 -demuxer h264es ffmpeg://tcp://192.168.0.115:2222
    //raspivid -t 0 -w 1280 -h 720 -hf -ih -fps 10 -o - | nc -k -l 2222
    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextureView textureView = view.findViewById(R.id.video_surface);
        textureView.setSurfaceTextureListener(this);
        final View errorView = view.findViewById(R.id.video_error_container);
        final TextView errorTextView = view.findViewById(R.id.video_error_text);
        final int colorWait = ContextCompat.getColor(view.getContext(), R.color.waitText);
        final int colorError = ContextCompat.getColor(view.getContext(), R.color.errorText);

        mSocketState.observe(this, new Observer<DecoderThread.State>() {
            @Override
            public void onChanged(@Nullable DecoderThread.State state) {
                if(state == null) return;
                switch (state) {
                    case CONNECTED:
                        errorView.setVisibility(View.GONE);
                        break;
                    case NOT_INITED:
                        errorView.setVisibility(View.VISIBLE);
                        errorTextView.setTextColor(colorWait);
                        errorTextView.setText(R.string.video_connection_wait);
                        break;
                    case RECONNECTING_ON_ERROR:
                        errorView.setVisibility(View.VISIBLE);
                        errorTextView.setTextColor(colorError);
                        errorTextView.setText(R.string.video_connection_fail);
                        break;
                    case DISCONNECTED:
                        errorView.setVisibility(View.VISIBLE);
                        errorTextView.setTextColor(colorWait);
                        errorTextView.setText(R.string.video_connection_ended);
                        break;
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        // create the decoder thread
        mDecoderThread = new DecoderThread(mSocketState);
        mDecoderThread.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mDecoderThread != null) {
            mDecoderThread.clear();
            mDecoderThread.interrupt();
            mDecoderThread = null;
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
        if (mDecoderThread != null) {
            mDecoderThread.setSurface(new Surface(surfaceTexture));
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        if (mDecoderThread != null) {
            mDecoderThread.setSurface(null);
        }
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

}
