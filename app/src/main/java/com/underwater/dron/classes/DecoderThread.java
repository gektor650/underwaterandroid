package com.underwater.dron.classes;

import android.arch.lifecycle.MutableLiveData;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.util.Log;
import android.view.Surface;

import com.underwater.dron.BuildConfig;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * @author gektor650
 * @since 11/7/17
 */


//https://github.com/ShawnBaker/RPiCameraViewer
//http://frozen.ca

public class DecoderThread extends Thread {
    private static final String CODEC_TYPE = "video/avc";

    // local constants
    private final static int TCPIP_BUFFER_SIZE = 16384;
    private final static int NAL_SIZE_INC = 4096;
    private final static int MAX_READ_ERRORS = 300;
    private static final String TAG = DecoderThread.class.getSimpleName();
    private final MutableLiveData<State> mThreadState;

    private Camera mCamera = new Camera(Source.ConnectionType.RawTcpIp, "", BuildConfig.IP, BuildConfig.VIDEO_PORT);

    // instance variables
    private MediaCodec decoder = null;
    private MediaFormat format;
    private boolean decoding = false;
    private Surface surface;
    private Source source = null;
    private ByteBuffer[] inputBuffers = null;
    private long presentationTime;
    private long presentationTimeInc = 66666;
    private RawH264Reader reader = null;

    public DecoderThread(MutableLiveData<State> stateMutableLiveData) {
        mThreadState = stateMutableLiveData;
        mThreadState.setValue(State.NOT_INITED);
    }

    public void setSurface(Surface surface) {
        this.surface = surface;
        if (decoder != null) {
            if (surface != null) {
                boolean newDecoding = decoding;
                if (decoding) {
                    setDecodingState(false);
                }
                if (format != null) {
                    try {
                        decoder.configure(format, surface, null, 0);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    if (!newDecoding) {
                        newDecoding = true;
                    }
                }
                if (newDecoding) {
                    setDecodingState(true);
                }
            } else if (decoding) {
                setDecodingState(false);
            }
        }
    }

    private synchronized void setDecodingState(boolean newDecoding) {
        try {
            if (newDecoding != decoding && decoder != null) {
                if (newDecoding) {
                    decoder.start();
                } else {
                    decoder.stop();
                }
                decoding = newDecoding;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        byte[] nal = new byte[NAL_SIZE_INC];
        int nalLen = 0;
        int numZeroes = 0;
        int numReadErrors = 0;
        // create the decoder
        try {
            decoder = MediaCodec.createDecoderByType(CODEC_TYPE);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        // create the reader
        source = mCamera.getCombinedSource();
        byte[] buffer = new byte[TCPIP_BUFFER_SIZE];
        first:
        while (true) {
            try {
                if (isInterrupted()) break;
                reader = new TcpIpReader(source);
                if (!reader.isConnected()) {
                    mThreadState.postValue(State.RECONNECTING_ON_ERROR);
                    sleep(2000);
                    continue;
                }
                mThreadState.postValue(State.CONNECTED);
                // read from the source
                while (!isInterrupted()) {
                    // read from the stream
                    int len = reader.read(buffer);
                    if (isInterrupted()) break first;

                    // process the input buffer
                    if (len > 0) {
                        numReadErrors = 0;
                        for (int i = 0; i < len && !isInterrupted(); i++) {
                            // add the byte to the NAL
                            if (nalLen == nal.length) {
                                nal = Arrays.copyOf(nal, nal.length + NAL_SIZE_INC);
                            }
                            nal[nalLen++] = buffer[i];

                            // look for a header
                            if (buffer[i] == 0) {
                                numZeroes++;
                            } else {
                                if (buffer[i] == 1 && numZeroes == 3) {
                                    if (nalLen > 4) {
                                        int nalType = processNal(nal, nalLen - 4);
                                        if (isInterrupted()) break;
                                        if (nalType == -1) {
                                            nal[0] = nal[1] = nal[2] = 0;
                                            nal[3] = 1;
                                        }
                                    }
                                    nalLen = 4;
                                }
                                numZeroes = 0;
                            }
                        }
                    } else {
                        numReadErrors++;
                        if (numReadErrors >= MAX_READ_ERRORS) {
                            closeReader();
                            break;
                        }
                    }

                    // send an output buffer to the surface
                    if (format != null && decoding) {
                        if (isInterrupted()) break;
                        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
                        int index;
                        do {
                            index = decoder.dequeueOutputBuffer(info, 0);
                            if (isInterrupted()) break;
                            if (index >= 0) {
                                decoder.releaseOutputBuffer(index, true);
                            }
                            //Log.info(String.format("dequeueOutputBuffer index = %d", index));
                        } while (index >= 0);
                    }
                }
            } catch (Exception ex) {
                Log.d(TAG, "Reconnect: " + ex.toString());
                closeReader();
            }
        }

        closeReader();
        stopDecoder();
        mThreadState.postValue(State.DISCONNECTED);
    }

    private void stopDecoder() {
        // stop the decoder
        if (decoder != null) {
            try {
                setDecodingState(false);
                decoder.release();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            decoder = null;
        }
    }

    private void closeReader() {
        // close the reader
        if (reader != null) {
            try {
                reader.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            reader = null;
        }
    }

    private int processNal(byte[] nal, int nalLen) {
        int nalType = (nalLen > 4 && nal[0] == 0 && nal[1] == 0 && nal[2] == 0 && nal[3] == 1) ? (nal[4] & 0x1F) : -1;

        // process the first SPS record we encounter
        if (nalType == 7 && !decoding) {
            SpsParser parser = new SpsParser(nal, nalLen);
            int width = (source.width != 0) ? source.width : parser.width;
            int height = (source.height != 0) ? source.height : parser.height;
            format = MediaFormat.createVideoFormat(CODEC_TYPE, width, height);
            if (source.fps != 0) {
                format.setInteger(MediaFormat.KEY_FRAME_RATE, source.fps);
                presentationTimeInc = 1000000 / source.fps;
            } else {
                presentationTimeInc = 66666;
            }
            presentationTime = System.nanoTime() / 1000;
            if (source.bps != 0) {
                format.setInteger(MediaFormat.KEY_BIT_RATE, source.bps);
            }
            decoder.configure(format, surface, null, 0);
            setDecodingState(true);
            inputBuffers = decoder.getInputBuffers();
            hideMessage();
            //VIDEO STARTED
        }

        // queue the frame
        if (nalType > 0 && decoding) {
            int index = decoder.dequeueInputBuffer(0);
            if (index >= 0) {
                ByteBuffer inputBuffer = inputBuffers[index];
                //ByteBuffer inputBuffer = decoder.getInputBuffer(index);
                inputBuffer.put(nal, 0, nalLen);
                decoder.queueInputBuffer(index, 0, nalLen, presentationTime, 0);
                presentationTime += presentationTimeInc;
            }
        }
        return nalType;
    }

    private void hideMessage() {

    }

    public void clear() {
        closeReader();
        stopDecoder();
    }

    public enum State {
        CONNECTED, NOT_INITED, RECONNECTING_ON_ERROR, DISCONNECTED
    }
}
