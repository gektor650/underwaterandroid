// Copyright © 2016 Shawn Baker using the MIT License.
package com.underwater.dron.classes;

import java.io.IOException;

public class RawH264Reader {
    protected Source source;

    public RawH264Reader(Source source) {
        this.source = source;
    }

    public int read(byte[] buffer) throws IOException{
        return 0;
    }

    public boolean isConnected() {
        return false;
    }

    public void close() {
    }
}
