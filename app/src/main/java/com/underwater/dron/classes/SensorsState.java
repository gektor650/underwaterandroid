package com.underwater.dron.classes;

import com.google.gson.Gson;
import com.underwater.dron.SensorData;

/**
 * @author gektor650
 * @since 1/11/18
 */

public class SensorsState {
    private float temperature;
    private SensorData gyroscope;
    private Gson gson = new Gson();

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public void setGyroscope(String gyroscopeString) {
        gyroscope = gson.fromJson(gyroscopeString, SensorData.class);
    }

    public SensorData getGyroscope() {
        return gyroscope;
    }
}
