// Copyright © 2016 Shawn Baker using the MIT License.
package com.underwater.dron.classes;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class TcpIpReader extends RawH264Reader {
    // public constants
    private final static int IO_TIMEOUT = 10000;

    // local constants
    private final static int CONNECT_TIMEOUT = 1000;
    private static final String TAG = TcpIpReader.class.getSimpleName();

    // instance variables
    private Socket socket = null;
    private InputStream inputStream = null;

    TcpIpReader(Source source) {
        super(source);
        try {socket = getConnection(source.address, source.port, CONNECT_TIMEOUT);
            if(socket != null) {
                socket.setSoTimeout(IO_TIMEOUT);
                inputStream = socket.getInputStream();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int read(byte[] buffer) throws IOException {
        return (inputStream != null) ? inputStream.read(buffer) : 0;
    }

    public boolean isConnected() {
        return (socket != null) && socket.isConnected();
    }

    public void close() {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            inputStream = null;
        }
        if (socket != null) {
            try {
                socket.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            socket = null;
        }
    }

    private static Socket getConnection(String baseAddress, int port, int timeout) {
        Socket socket;
        try {
            socket = new Socket();
            InetSocketAddress socketAddress = new InetSocketAddress(baseAddress, port);
            socket.connect(socketAddress, timeout);
        } catch (Exception ex) {
            Log.d(TAG, "Cant connect to socket " + baseAddress + ":" + port);
            socket = null;
        }
        return socket;
    }
}
